'use strict'
const electron = require('electron')
const path = require('path')
const {ipcMain, globalShortcut} = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const os = require('os');

const languageStorageKey = 'np.lang'
let mainWindow
let config = {}


if (process.env.NODE_ENV === 'development') {
  config = require('../config')
  config.url = `http://localhost:${config.port}`
} else {
  config.devtron = false
  config.url = `file://${__dirname}/dist/index.html`
}

/**
 * Menu
 */
var setLang = function (lang) {
    mainWindow.webContents.send('lang-update', lang);


}

const template = [
    {
        label: "Electron",
        submenu: [
            // { label: "English", click: setLang('en')},
            // { label: "Dutch", click: setLang('nl')},
            // { label: "French", click: setLang('fr')},
            // { label: "German", click: setLang('de')},
        ]
    },
    {
        label: "Language",
        submenu: [
            { label: "English", click: function(item, focusedWindow) {
                setLang('en')
            }},
            { label: "Dutch", click: function(item, focusedWindow) {
                setLang('nl')
            }},
            { label: "French", click: function(item, focusedWindow) {
                setLang('fr')
            }},
            { label: "German", click: function(item, focusedWindow) {
                setLang('de')
            }},
        ]
    },
  {
    label: 'View',
    submenu: [
      {
        label: 'Restart Neurocoach',
        click (item, focusedWindow) {
          mainWindow.webContents.send('menu-restart-neurocoach', 1)
        }
      },
      {
        label: 'Dimmer Speed',
        submenu: [
          {
            label: '1 (slowest)',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 1)
            }
          },
          {
            label: '2',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 2)
            }
          },
          {
            label: '3',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 3)
            }
          },
          {
            label: '4',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 4)
            }
          },
          {
            label: '5',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 5)
            }
          },
          {
            label: '6',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 6)
            }
          },
          {
            label: '7',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 7)
            }
          },
          {
            label: '8',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 8)
            }
          },
          {
            label: '9 (fastest)',
            click (item, focusedWindow) {
              mainWindow.webContents.send('set-dimmer', 9)
            }
          },

        ]
      },
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click (item, focusedWindow) {
          if (focusedWindow) focusedWindow.reload()
        }
      },
      {
        label: 'Toggle Developer Tools',
        accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
        click (item, focusedWindow) {
          if (focusedWindow) focusedWindow.webContents.toggleDevTools()
        }
      },
    ]
  },

  {
    role: 'window',
    submenu: [
      {
        role: 'minimize'
      },
      {
        role: 'close'
      }
    ]
  },
  {
    role: 'help',
    submenu: [
      {
        label: 'Learn More',
        click () { require('electron').shell.openExternal('http://electron.atom.io') }
      }
    ]
  }
]



function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 800,
    width: 1226,
    webPreferences: {
      plugins: true
    }
  })

  mainWindow.loadURL(config.url)

  if (process.env.NODE_ENV === 'development') {
    BrowserWindow.addDevToolsExtension(path.join(__dirname, '../node_modules/devtron'))

    let installExtension = require('electron-devtools-installer')

    installExtension.default(installExtension.VUEJS_DEVTOOLS)
        .then((name) => mainWindow.webContents.openDevTools())
        .catch((err) => console.log('An error occurred: ', err));
    console.log('devtools should have opened.');
  }

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  // Create Shortcut Keys
  globalShortcut.register('Alt+Up', () => {
    mainWindow.webContents.send('speed-up-dimmer')
  })
  globalShortcut.register('Alt+Down', () => {
    mainWindow.webContents.send('slow-down-dimmer')
  })



  electron.Menu.setApplicationMenu(electron.Menu.buildFromTemplate(template))
}

let pluginName = '';
switch (process.platform) {
  case 'win32':
    pluginName = 'pepflashplayer32_23_0_0_162.dll'
    break
  case 'darwin':
    pluginName = 'PepperFlashPlayer.plugin'
    break
  case 'linux':
    pluginName = 'libpepflashplayer.so'
    break
}
var flashPath = path.join(__dirname, pluginName);
console.log(flashPath);
app.commandLine.appendSwitch('ppapi-flash-path', path.join(__dirname+'/plugins', pluginName))

// Optional: Specify flash version, for example, v17.0.0.169
app.commandLine.appendSwitch('ppapi-flash-version', '23.0.0.162')


app.on('ready', createWindow)

app.on('window-all-closed', () => {
  globalShortcut.unregisterAll();
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
