import Vue from 'vue'
import Electron from 'vue-electron'
import Resource from 'vue-resource'
import Router from 'vue-router'
import App from './App'
import routes from './routes'
import ClientFactory from './plugins/Websockets/ClientFactory.js'
import Server from './plugins/Websockets/Server.js'
import Dimmer from './plugins/Dimmer.js'
import Neurocoach from './plugins/Neurocoach.js'
import NeurocoachLauncher from './plugins/launchers/NeurocoachLauncher.js'
import ActivityManager from './plugins/ActivityManager.js'
import Storage from './plugins/Storage'
import SoundManager from './plugins/SoundManager'
import NeurocoachLeadPlacementCalculator from './plugins/NeurocoachLeadPlacementCalculator.js'
import Lang from './plugins/Localization.js'

// const Store = require('electron-store');
// const store = new Store();
// console.log(Lang);
Lang.requireAll(require.context('./lang', true, /\.js$/));

Vue.use(Lang, {
    lang: 'en'
});
console.log(Vue.$lang)
Vue.use(ClientFactory);
Vue.use(Server);
Vue.use(Dimmer);
Vue.use(Neurocoach);
Vue.use(ActivityManager);
Vue.use(Storage);
Vue.use(NeurocoachLauncher);
Vue.use(NeurocoachLeadPlacementCalculator);
Vue.use(Electron)
Vue.use(SoundManager)
Vue.use(Resource)
Vue.use(Router)

Vue.config.debug = true

const router = new Router()

router.map(routes)
router.beforeEach(() => {
  window.scrollTo(0, 0)
})
router.redirect({
  '*': 'login'
})

router.start(App, 'app')
