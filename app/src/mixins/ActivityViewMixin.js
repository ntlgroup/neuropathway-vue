export default {
    data () {
        return {
            tcpClient: null,
        }
    },

    ready () {
        if(this.shared.coach.pin) {
            this.initNeurocoach()
        }

        this.$on('neurocoach.restart', function() {
            this.tcpClient.destroy();
            console.log('restarting NC');
            this.initNeurocoach();
        });
    },
    destroyed () {
        if(this.tcpClient) {
            this.tcpClient.destroy();
        }
        delete this.tcpClient;
        this.$dispatch('neurocoachConnector.kill')
        this.$dimmer.stop()
    },

    methods: {
        socketError() {
            console.log('client error, reconnecting');
            this.tcpClient.destroy();
            this.tcpClient = null;
            this.resetTcpClient();
            window.setTimeout(function() {
                this.$dispatch('neurocoachLauncher.launched', this.tcpClient);
                this.shared.neurocoach.connected = true;
            }.bind(this), 50);
        },
        resetTcpClient () {
            var vm = this;
            this.shared.neurocoach.connected = false;
            this.tcpClient = this.$tcpClientFactory.get();
            // console.log('adding callbacks');
            this.tcpClient.registerErrorCallback(this.socketError)
            this.tcpClient.registerInThresholdCallback(function() {
                vm.onInThreshold()
            });
            this.tcpClient.registerOutOfThresholdCallback(function() {
                vm.onOutOfThreshold()
            });
        },
        initNeurocoach () {
            var current = this.shared.activity.currentActivity;
            // Set TCP Client callbacks
            var vm = this;

            if(this.shared.coach.pin) {
                vm.resetTcpClient();

                this.$dispatch('neurocoachLauncher.launched', this.tcpClient);
                if(current && current.protocol && process.platform == 'win32') {
                    // Start Neurocoach
                    this.$neurocoachLauncher.startWithProtocol(current.protocol).then(function() {
                        window.setTimeout(function() {
                            this.$dispatch('neurocoachLauncher.launched', this.tcpClient);
                            this.shared.neurocoach.connected = true;
                        }.bind(this), 50);

                    }.bind(this));
                    console.log('Starting with Neurocoach')
                }

            }else {
                console.log('Starting w/o Neurocoach')
            }
        },
    }
}