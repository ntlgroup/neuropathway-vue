#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Res_Comment=http://www.hiddensoft.com/autoit3/compiled.html
#AutoIt3Wrapper_Res_Description=AutoIt v3 Compiled Script
#AutoIt3Wrapper_Run_AU3Check=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#Region converted Directives from C:\Program Files\Neurocoach-dev\DimmerGames\runGame.au3.ini
#EndRegion converted Directives from C:\Program Files\Neurocoach-dev\DimmerGames\runGame.au3.ini
;
#include <guiconstants.au3>
#include <windowsconstants.au3>
#include <misc.au3>
#include <IE.au3>

$iniFile = @ScriptDir&"\client_id.ini"
$clientID = IniRead($iniFile, 'neurocoach', 'clientId', '65500170')
ConsoleWrite($clientID&@CRLF)

$w = 1024;
$h = 768;

Local $oIE = _IECreateEmbedded()
GUICreate("NC Arrows",800, 600, _
        (@DesktopWidth - $w) / 2, (@DesktopHeight - $h) / 2, _
        $WS_OVERLAPPEDWINDOW + $WS_CLIPSIBLINGS + $WS_CLIPCHILDREN)
GUICtrlCreateObj($oIE, 0, 0, $w, $w)
GUISetState(@SW_SHOW) ;Show GUI

$url = "http://portal.myhealthybrain.net/activities/arrows/Arrows.php?client_id="&$clientID

_IENavigate($oIE, $url)
_IEAction($oIE, "stop")

; Waiting for user to close the window
While 1
    Local $msg = GUIGetMsg()
    Select
        Case $msg = $GUI_EVENT_CLOSE
            ExitLoop

    EndSelect
WEnd

GUIDelete()

Exit

Func CheckError($sMsg, $error, $extended)
    If $error Then
        $sMsg = "Error using " & $sMsg & " button (" & $extended & ")"
    Else
        $sMsg = ""
    EndIf
    GUICtrlSetData($GUI_Error_Message, $sMsg)
EndFunc   ;==>CheckError
