#include <File.au3>


Global $basePath = "C:\Program Files\Neurocoach-dev\NtlGroup\Protocols\"


$fileList = _FileListToArray($basePath&"bak")

HotKeySet("{ESC}", "_END")
Func _END()
	Exit
EndFunc

; Activate BioEra
; _WinWaitActivate("BioEra 3.055","")


AutoItSetOption("MouseCoordMode", 2)

For $i = 1 To $fileList[0]
	; ConsoleWrite($fileList[$i]&@CRLF)
	$filename = $fileList[$i]
	$strippedFilename = StringStripWS($filename, 8)

	if not FileExists($basePath&$strippedFilename) Then ConsoleWrite($filename&@CRLF)


Next



Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc


