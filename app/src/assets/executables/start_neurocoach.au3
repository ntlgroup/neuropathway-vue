#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=..\start_neurocoach.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.12.0
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
$process = 'bioera.exe';
While ProcessExists($process)
	ProcessClose($process)
	Exit
WEnd

$numberOfArgs = $CmdLine[0];

if $numberOfArgs <> 9 Then
	MsgBox(0, 'alert', 'Not Enough Arguments')
Else
	$command = 'c:\program files\neurocoach\bioera.exe'
	$command = $command & ' -or -design NtlGroup\Design\neurocoach-autoprotocol.bpd '
	$command = $command & $CmdLine[1] & ' ' & $CmdLine[2] &' ' & $CmdLine[3] &' ' & $CmdLine[4] &' ' & $CmdLine[5] &' ' & $CmdLine[6] &' ' & $CmdLine[7] &' ' & $CmdLine[8];

	; MsgBox(0, '', $command);

	Run($command, 'c:\program files\neurocoach');

	WinActivate("NeuroPathway")
	WinSetOnTop("NeuroPathway", "", 1)

	Sleep(10000)
	WinSetOnTop("NeuroPathway", "", 0)

Endif