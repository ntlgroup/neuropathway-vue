export default {
    // User
    username: '',
    password: '',
    user: {
        id: 65500050,
        first_name: 'Justin'
    },

    neurocoach: {
        url: 'localhost',
        port: 7489,
        connected: false,
        inThreshold: false
    },
    dimmer: {
        enabled: false,
        paused: true,
        speed: 15
    },

    system: {},

    coach: {
        pin: null,
    },

    // Sidebar
    versionString: '1.0',
    timerLabel: '0:00',
    timeText: '0:00',

    activity: {
        activities: [],
        playlist: [],
        currentActivity: null,
        nextActivity: null,
        currentActivity: null,
        currentActivityIndex: null,
    }
}