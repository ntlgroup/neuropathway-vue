export default {
    // System
    versionString: '2.0.8',
    licenseCode: '',

    // User
    username: '',
    password: '',
    user: {
        id: null,
        first_name: '',
    },

    neurocoach: {
        url: 'localhost',
        port: 7489,
        connected: false,
        inThreshold: false,
        messages: [],
    },
    dimmer: {
        enabled: false,
        paused: true,
        speed: 5
    },

    system: {},

    coach: {
        pin: null,
    },

    // Sidebar
    timerLabel: '0:00',
    timeText: '0:00',
    playlist: [],
    volume: 10,
    activity: {
        activities: [],
        currentActivity: null,
        nextActivity: null,
        currentActivity: null,
        currentActivityIndex: null,
    }
}