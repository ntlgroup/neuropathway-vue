import Vue from 'vue'

export default {
  'login': {
    component: Vue.component('login', require('./components/Login')),
    name: 'login'
  },
  'activitySetup': {
    component: Vue.component('activitySetup', require('./components/ActivitySetup')),
    name: 'activitySetup'
  },
  'activity-iframe': {
    component: Vue.component('activityIFrame', require('./components/ActivityIFrame')),
    name: 'activityIFrame'
  },

  'activity-blank': {
    component: Vue.component('activityBlank', require('./components/ActivityBlank')),
    name: 'activityBlank'
  },

  'activity-music-stopgo': {
    component: Vue.component('activityMusicStopGo', require('./components/ActivityMusicStopGo')),
    name: 'activityMusicStopGo'
  },

    'activity-youtube': {
        component: Vue.component('activityYoutube', require('./components/ActivityYoutube')),
        name: 'activityYoutube'
    },

    'activity-coolmath': {
        component: Vue.component('activityCoolmath', require('./components/ActivityCoolmath.vue')),
        name: 'activityCoolmath'
    },
}
