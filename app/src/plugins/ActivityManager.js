let plugin = {}

plugin.install = (Vue, options) => {

    Vue.prototype.$activityManager = {
        activities: [],
        playlist: [],
        currentActivity: {activity: {name: 'None'}},
        nextActivity: {activity: {name: 'None'}},
        currentActivityIndex: 0,

        setActivities (activities) {
            this.activities = activities;
            this.currentActivity = this.activities[0]
            this.nextActivity = this.activities[1];
            //console.log('activities set');
            //console.log(this);
        },
        getCurrentActivityName () {
            return this.currentActivity.activity.name;
        },

        goToNextActivity () {
            if (this.currentActivityIndex == this.activities.length - 1) {
                return false;
            }
            this.currentActivityIndex = this.currentActivityIndex + 1;
            this.currentActivity = this.activities[this.currentActivityIndex];
            this.updatePreviousAndNext();
        },
        goToPreviousActivity () {
            if (this.currentActivityIndex == 0) {
                return false;
            }
            this.currentActivityIndex = this.currentActivityIndex - 1;
            this.currentActivity = this.activities[this.currentActivityIndex];
            this.updatePreviousAndNext();
        },
        updatePreviousAndNext () {
            this.nextActivity = (this.activities.length >= this.currentActivityIndex + 2) ? this.activities[this.currentActivityIndex + 1] : null;
            this.previousActivity = (this.currentActivityIndex > 0) ? this.activities[this.currentActivityIndex - 1] : null;
        },
    }

}
export default plugin;
