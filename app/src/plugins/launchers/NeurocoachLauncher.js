import ProcessLauncher from './launcher'
var Promise = require('promise');
let plugin = {}

plugin.install = (Vue, options) => {

    Vue.prototype.$neurocoachLauncher = {
        launcher: new ProcessLauncher('start_neurocoach.exe', false),
        isRunning: false,
        autostart: false,

        kill () {
            this.launcher.kill();
            this.isRunning = false;
        },

        startWithProtocol (protocol) {
            //console.log('Starting Neurocoach');


            var promise = new Promise(function(resolve, reject) {
                this.launcher.executeWithPromiseOnExit([
                    protocol.file_template,
                    protocol.go_1_low, protocol.go_1_high,
                    protocol.go_2_low, protocol.go_2_high,
                    protocol.stop_1_low, protocol.stop_1_high,
                    protocol.stop_2_low, protocol.stop_2_high,
                ]).then(function() {
                    this.isRunning = true;
                    resolve('success');
                }.bind(this));
            }.bind(this));
            return promise;
        },

        startExternalDimmer (type, option) {
            var launcher = new ProcessLauncher('dimmer-cli.exe', false);
            launcher.execute([type, option]);
        },
    }

}
export default plugin;
