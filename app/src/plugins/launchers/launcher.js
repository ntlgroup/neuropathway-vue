const path = require('path')
var Promise = require('promise');

var ProcessLauncher = function(executable, autoStart) {
    if (typeof autoStart === 'undefined') {
        autoStart = true;
    }
    this.autostart = autoStart;
    this.file = executable;
    this.execFile = require('child_process').execFile;
    this.child = null;
    this.basePath = path.join('resources\\app\\executables', this.file);
    if(process.env.NODE_ENV === 'development') {
        this.basePath = path.join('C:\\Temp\\neuropathway-vue-2\\app\\executables', this.file);
    }
    console.log(this.basePath);

    this.execute = function(args) {
        var path = this.basePath;

        var _args = [];
        for(var i in args) {
            if (args[i]) {
                _args.push(args[i])
            }else {
                _args.push(0);
            }
        }

        //console.log(_args);
        //console.log('Executing ' + path + ' with ' + _args.length + ' arguments.');

        this.child = this.execFile(path, args, function(error, stdout, stderr) {
            if (error) {
                //console.log(error.stack);
                //console.log('Error code: ' + error.code);
                //console.log('Signal received: ' + error.signal);
            }
            //console.log('Child Process stdout: '+ stdout);
            //console.log('Child Process stderr: '+ stderr);
        });
        this.child.on('exit', function (code) {
            console.log('Child process exited '+ 'with exit code '+ code);
        });
    }

    if (this.autostart) {
        this.execute();
    }

    this.executeWithPromiseOnExit = function(args) {
        var path = this.basePath;
        console.log(path);
        console.log('inside launcher.js');
        var _args = [];
        for(var i in args) {
            if (args[i]) {
                _args.push(args[i])
            }else {
                _args.push(0);
            }
        }

        //console.log(_args);
        //console.log('Executing ' + path + ' with ' + _args.length + ' arguments with promise on exit.');
        var promise = new Promise(function(resolve, reject) {
            this.child = this.execFile(path, args, function(error, stdout, stderr) {
                if (error) {
                    //console.log(error.stack);
                    //console.log('Error code: ' + error.code);
                    //console.log('Signal received: ' + error.signal);
                }
                //console.log('Child Process stdout: '+ stdout);
                //console.log('Child Process stderr: '+ stderr);
                return reject(stderr);
            }.bind(this));
            this.child.on('exit', function (code) {
                //console.log('Child process exited '+ 'with exit code '+ code);
                return resolve('Process ended.');
            }.bind(this));
        }.bind(this));
        return promise;
    }

    this.write = function(input) {
        this.child.stdin.write(input);
    }

    this.kill = function() {
        this.child.kill();
    }
}

module.exports = ProcessLauncher