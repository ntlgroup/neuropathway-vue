let plugin = {}

plugin.install = (Vue, options) => {
    var servers = [];
    var transparency = 0;
    Vue.prototype.$dimmer = {
        dimAmount: 0.01,
        opacity: 0.5,
        interval: 100,
        dimming: false,
        paused: true,
        element: null,
        $scope: null,
        initialized: false,
        callbacks: [],
        dimInterval: 0,

        addCallback (callback) {
            this.callbacks.push(callback);
        },
        removeCallbacks () {
            this.callbacks = [];
        },

        setElement (element) {
            this.element = element;
        },

        fake () {

            this.start();
        },

        setScope (scope) {
            this.$scope = scope;
        },

        togglePause  () {
            this.paused = true;
        },

        stop  () {
            this.dimming = false;
            // console.log('clearing interval');
            window.clearInterval(this.dimInterval)
            // window.clearInterval(this.interval);

        },
        start  () {
            // console.log('Dimmer.js: starting dimmer');
            var self = this;
            this.dimInterval = setInterval(function () {
                self.process();
            }, this.interval);
            this.paused = false;
        },
        process  () {

            // console.log('Dimmer.js@Processing: Paused, then Dimming')
            // console.log(this.paused);
            // console.log(this.dimming)
            // console.log('Dimmer.js@Processing: Paused, then Dimming')
            if (this.paused) {
                this.undim();
                return;
            }
            if (this.dimming) {
                this.dim();
            } else {
                this.undim();
            }
        },

        update (cb) {
            // console.log('sending transparency back to callback: ' + this.transparency)
            // cb(this.transparency);
            // console.log(this.callbacks);
            for (var i in this.callbacks) {
                this.callbacks[i](this.opacity);
            }
        },

        /*
           Dimming
         *
         * 1 is the darkest dimming value, or when under thresh
         * 0 is the lightest dimming value, or when over thresh
         * Dimming is unwanted, what happends when under thresh
         */
        undim  () {
            this.opacity = this.opacity - this.dimAmount;
            if(this.opacity <= 0) this.opacity = 0;
            this.update();
            // console.log('Dimmer.js: Dim | Transparency' + this.opacity);
        },

        // 1 is the highest or darkest dimming value
        dim  () {
            this.opacity = this.opacity + this.dimAmount;
            if(this.opacity >= 0.7) this.opacity = 0.7;
            this.update();
            // console.log('Dimmer.js: Undim | Transparency' + this.opacity);
        },

        toggleDirection  () {
            this.dimming = !this.dimming;
        },

        setDirection (direction) {
            // console.log('Dimmer.js: Direction Change | ' + direction);
            if(direction == 1) {
                this.dimming = false;
            }else {
                this.dimming = true;
            }
        },

        increaseSpeed  () {
            if (this.dimAmount <= 0.02) {
                this.dimAmount = this.dimAmount + 0.002;
            }
            // console.log('Dimmer.js: Speed Change | ' + this.dimAmount);
        },

        decreaseSpeed  () {
            if (this.dimAmount > 0.002) {
                this.dimAmount = this.dimAmount - 0.002;
            }
        },

        setSpeed (speed) {
            switch(speed) {
                case 1:
                    this.dimAmount = 0.0007
                    break
                case 2:
                    this.dimAmount = 0.0009
                    break
                case 3:
                    this.dimAmount = 0.001
                    break
                case 4:
                    this.dimAmount = 0.003
                    break
                case 5:
                    this.dimAmount = 0.005
                    break
                case 6:
                    this.dimAmount = 0.007
                    break
                case 7:
                    this.dimAmount = 0.009
                    break
                case 8:
                    this.dimAmount = 0.01
                    break
                case 9:
                    this.dimAmount = 0.02
                    break
                case 10:
                    this.dimAmount = 0.05
                    break
            }
        },
    }

}
export default plugin;
