/*
 *	Defines the Plugin Class
 */
import store from '../vuex/store'

var Plugin = function(){ };
var Lang = function(options){
    this.setLang(options.lang);
};

//This static object will hold all lang files
Lang.files = {};

/*
 *	This method is used to set the current lang.
 */
Lang.prototype.setLang = function(lang){
    store.commit('SET_LANGUAGE', lang);
    //This will hold the current lang
    this.current_lang = lang;
    //Holds the lag data
    this.lang_data = {};
    console.log(this);

    for(var i in Lang.files){
        if(i.indexOf('../lang/'+this.current_lang+'/') === 0){
            var lang_name = i.replace('./'+this.current_lang+'/', '').replace('.js', '');
            this.lang_data[lang_name] = Lang.files[i];
        }
    }

    this.current_lang = lang;
    this.lang_data = {};

    for(var i in Lang.files){
        if(i.indexOf('./'+lang+'/') === 0){
            var lang_name = i.replace('./'+lang+'/', '').replace('.js', '');
            this[lang_name] = Lang.files[i];
        }
    }
};

Lang.prototype.get = function(key) {
    if(store.state.lang[store.state.lang.current_lang] && store.state.lang[store.state.lang.current_lang][key]) {
        return store.state.lang[store.state.lang.current_lang][key];
    }
    return store.state.lang['en'][key]

}

/*
 *	Gets the current lang
 */
Lang.prototype.getLang = function(){
    return this.current_lang;
};

/*
 *	Requires all langs file
 */
Plugin.requireAll = function(r) {
    r.keys().forEach(function(d){
        Lang.files[d] = r(d);
    });
};

/*
 *	Installs the plugin in the vuejs.
 */
Plugin.install = function(Vue, options){
    var o = options || {};

    var default_lang = o.default || 'en';

    Vue.prototype.$lang = new Lang(options);
    // Object.defineProperty(Vue.prototype, '$lang', {
    //     get:function () { return this.$root._lang }
    // });

    if(typeof o != 'object'){
        console.error('[vue-lang] the options should be an object type.');
        return false;
    }

    Vue.mixin({
        // beforeCompile:function() {
        //     Vue.util.defineReactive(this, '_lang',  new Lang({lang:default_lang}));
        // }
    });
};

export default Plugin;