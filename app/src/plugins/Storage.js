let plugin = {}

plugin.install = (Vue, options) => {

    Vue.prototype.$storage = {
        put (key, value)
        {
            localStorage[key] = value;
        },

        get (key)
        {
            return localStorage[key];
        },

        unset (key)
        {
            localStorage.removeItem(key);
        },
    }

}
export default plugin;
