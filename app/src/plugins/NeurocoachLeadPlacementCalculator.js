let plugin = {}

plugin.install = (Vue, options) => {

    Vue.prototype.$ncPlacement = {
        generateFromProtocol (protocol) {
            var html = '';
            if ( protocol.ch_1_placement) {
                html = html + this.getCircleHtml('ch1', protocol.ch_1_placement.toLowerCase());
            }
            if ( protocol.ref_1_placement) {
                html = html + this.getCircleHtml('ref1', protocol.ref_1_placement.toLowerCase());
            }
            if ( protocol.ch_2_placement) {
                html = html + this.getCircleHtml('ch2', protocol.ch_2_placement.toLowerCase());
            }
            if ( protocol.ref_2_placement) {
                html = html + this.getCircleHtml('ref2', protocol.ref_2_placement.toLowerCase());
            }
            if ( protocol.ground_placement) {
                html = html + this.getCircleHtml('ground', protocol.ground_placement.toLowerCase());
            }
            return html;
        },
        getCircleHtml (type, position)
        {
            var markerHtml;

            switch(position) {
                case 'le':
                    markerHtml = "<div class='marker a1 "+type+"' ></div><div class='marker a2 "+type+"' ></div>"
                    break;

                case 'mass':
                    markerHtml = "<div class='marker mass1 "+type+"' ></div><div class='marker mass2 "+type+"' ></div>"
                    break;

                default:
                    markerHtml = "<div class='marker "+position+" "+type+"' ></div>"
            }

            return markerHtml;

        }
    }

}
export default plugin;
