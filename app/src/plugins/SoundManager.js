import SM from 'soundmanager2'

let plugin = {}

plugin.install = (Vue, options) => {

    Vue.prototype.$soundmanager = {

        initialized: false,
        soundManager: SM.soundManager,

        init () {
            var sm = this;
            this.soundManager.setup({
                url: '../../node_modules/soundmanager2/swf/'
            })
        },

        addSound(id, url, onfinish, whileplaying) {
            console.log('Adding track ' + id + ' from url: ' + url);
            return this.soundManager.createSound({
                id: id,
                url: url,
                onfinish: function() {
                    if(onfinish) {
                        onfinish(this)
                    }
                },
                whileplaying: function() {
                    if(whileplaying) {
                        whileplaying(this)
                    }
                }
            });
        },

        setVolume(volume) {
            this.soundManager.setVolume(volume)
        },

        play(trackId) {
            console.log('playing ' + trackId);
            this.soundManager.play(trackId)
        },

        pause(trackId) {
            console.log('pausing ' + trackId);
            this.soundManager.pause(trackId)
        },

        stop(trackId) {
            this.soundManager.stop(trackId)
        }


    }
}
export default plugin;
