import {Client} from './Client.js'

let plugin = {}

plugin.install = (Vue, options) => {

    Vue.prototype.$tcpClientFactory = {
        clients: [],
        get () {
            return new Client
        },


    }
}

export default plugin;
