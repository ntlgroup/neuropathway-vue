import net from 'net'

export class Client {
    dataCallbacks = []
    errorCallbacks= []
    connectionCallbacks= []
    inThresholdCallbacks= []
    outOfThresholdCallbacks= []
    socket= new net.Socket()
    connected= false
    ipAddress= ''
    port= 9789
    shouldReconnect= true
    initialized= false
    retryConnectInterval= 0
    reconnectTries= 0

    registerDataCallback(callback) {
        this.dataCallbacks.push(callback)
    }

    registerErrorCallback(callback) {
        this.errorCallbacks.push(callback)
    }

    registerConnectedCallback (callback) {
        this.connectionCallbacks.push(callback);
    }

    registerInThresholdCallback (callback) {
        this.inThresholdCallbacks.push(callback);
    }

    registerOutOfThresholdCallback (callback) {
        this.outOfThresholdCallbacks.push(callback);
    }

    connect (port, ip) {
        this.port = port;
        this.ip = ip;

        this.destroy();

        this.socket.on('error', function(error) {
            // console.log('[NC Client] Error= ' + error)
            this.onError(error)
        }.bind(this))
        this.socket.on('data', function(data) {
            // console.log('[NC Client] Data')
            // console.log(data)
            this.onRead(data)
        }.bind(this))

        this.socket.on('close', function(data) {
            // console.log('[NC Client] Close')
            this.connected = false;
            this.onConnectionChange();
            // this.startTryingToReconnect();
        }.bind(this))

        this.socket.connect(port, ip, function() {
            this.connected = true;
            this.reconnectTries = 0;
            this.onConnectionChange();
        }.bind(this));

    }
    destroy() {
        this.socket.end();
        this.socket.destroy();
    }
    start () {
        this.connect(this.port, this.ip);
    }
    resetConnection () {
        this.ip = '192.168.1.134';
        this.port = '57318'
        this.connect(this.port, this.ip);
    }

    startTryingToReconnect () {
        this.clearReconnectInterval();
        var vm = this;
        if(this.shouldReconnect) {
            this.retryConnectInterval = window.setTimeout(function() {
                vm.reconnectTries = vm.reconnectTries + 1;
                //console.log('trying to connect= ' + vm.reconnectTries);
                vm.resetConnection();
            }.bind(this), 2000);
        }
    }

    clearReconnectInterval() {
        window.clearInterval(this.retryConnectInterval);
    }

    onConnectionChange () {
        for(var i in this.connectionCallbacks) {
            this.connectionCallbacks[i](this.connected);
        }
    }

    onEnd (self) {
        self.connected = false;
        this.onConnectionChange();
        // this.startTryingToReconnect();
    }

    onError (data, self) {
        var self = this;
        for(var i in this.errorCallbacks) {
            this.errorCallbacks[i](this.ab2str(data));
        }
        //console.log('restarting');
        // this.startTryingToReconnect();
    }

    ab2str(buf) {
        return String.fromCharCode.apply(null, buf);
    }

    onRead (data) {
        // console.log(data);
        for(var i in this.dataCallbacks) {
            this.dataCallbacks[i](data)
        }
        try {
            var code = data[0];
            if(code == 0) {
                for(var i in this.outOfThresholdCallbacks) {
                    this.outOfThresholdCallbacks[i]();
                }
            }else {
                for(var i in this.inThresholdCallbacks) {
                    this.inThresholdCallbacks[i]();
                }
            }
        }
        catch(e) {

        }
    }
}