import net from 'net'

let plugin = {}

plugin.install = (Vue, options) => {

    Vue.prototype.$tcpServer = {
        server: null,
        clients: [],
        ab2str(buf) {
            return String.fromCharCode.apply(null, new Uint16Array(buf));
        },
        listen (host, port, callback) {
            var vm = this;
            this.server = net.createServer(function(socket) {
                this.clients.push(socket);
                socket.on('data', function(data) {
                    //console.log('client connected');
                    callback(vm.ab2str(data));
                });
                // var test = new Buffer([231]);
                // socket.write(test);
            }.bind(this))
            this.server.listen({
                host: host,
                port: port,
            });
            //console.log('[Server] Listening')
        },
        close() {
            this.server.close();
            for(var i in this.clients) {
                this.clients[i].destroy();
            }
        }

    }

}
export default plugin;
