import * as types from '../mutation-types'

const state = {
    current_lang: 'en',
    en: {
        // Sidebar
        dim_speed: 'Dim Speed',
        back_to_login: 'Back to Login',
        time_remaining: 'Time Remaining',
        stopped: 'Stopped',
        volume: 'Volume',
        none: 'None',
        current_activity: 'Current Activity',
        up_next: 'Up Next',
        continue: 'Continue',
        done: 'Done',

        // Login
        username: 'Username',
        password: 'Password',
        login: 'Login',
        login_error: 'Sorry, your username or password was incorrect. Please try again.',
        update_prompt: 'An updated version of NeuroPathway is available.',
        update_cta: 'Update Now',
        loading: 'Loading...',


        // Activity Setup
        pin: 'PIN',
        pin_instructions: 'Please enter a pin number.  If you do not enter a pin number, you may still proceed, however the NeuroCoach protocols will not be activated. ',
        verify_pin: 'Verify PIN',
        invalid_pin: 'Invalid PIN. Please enter the PIN again, or continue without NeuroCoach',
        pin_verified: 'Pin Verified!',
        eeg_setup: 'Please set up your EEG device according to the chart to the right.  Once you have set up your EEG device, click the start button.  Your timer will start automatically!',
        protocol_change: 'There has been a protocol change for this activity.',
        dismiss_continue: 'Dismiss and Continue',

        // Colors
        red: 'Red',
        black: 'Black',
        green: 'Green',

        // Music
        eyes_closed: 'Eyes Closed',
        eyes_closed_instructions: 'Please keep your eyes closed for this activity.'
    },
    nl: {
        // Sidebar
        dim_speed: 'Dim Snelheid',
        back_to_login: 'Terug naar inloggen',
        time_remaining: 'Resterende tijd',
        stopped: 'Gestopt',
        volume: 'Volume',
        none: 'None',
        current_activity: 'Huidige activiteit',
        up_next: 'Volgende',
        continue: 'Continue',
        done: 'Klaar',

        // Login
        username: 'Gebruikersnaam',
        password: 'Wachtwoord',
        login: 'Login',

        // Activity Setup
        pin: 'PIN',
        pin_instructions: 'Gelieve een pincode in te geven. Als je geen pincode ingeeft kan je nog steeds doorgaan maar worden de Neurocoach protocols niet geactiveerd.',
        verify_pin: 'PIN verifiëren',
        invalid_pin: 'Onjuiste Pincode. Gelieve nogmaals de Pincode in te geven of ga door zonder NeuroCoach.',
        pin_verified: 'Pincode correct!',
        eeg_setup: 'Gelieve uw EEG apparaat aan te sluiten zoals aangegeven in de rechter tabel. Zodra u de EEG heeft aangesloten en klaar bent, gelieve op de start button te klikken.  Uw timer zal automatisch starten.',

        // Colors
        red: 'Red',
        black: 'Black',
        green: 'Green',

        // Music
        eyes_closed: 'Gesloten Ogen',
        eyes_closed_instructions: 'Gelieve uw ogen gesloten te houden voor deze activiteit'
    }
}

const mutations = {
    [types.SET_LANGUAGE] (state, lang) {
        console.log('setting language in store');
        state.current_lang = lang
    }
}

const getters = {
    trans: function(state, key) {
        return 'hey'
        return state[state.current_lang][key];
    }
}
export default {
    state,
    mutations,
    getters
}
