#include <File.au3>


Global $basePath = "C:\Program Files\Neurocoach-dev\NtlGroup\Protocols\"


$fileList = _FileListToArray($basePath&"bak")

HotKeySet("{ESC}", "_END")
Func _END()
	Exit
EndFunc

; Activate BioEra
; _WinWaitActivate("BioEra 3.055","")


AutoItSetOption("MouseCoordMode", 2)

For $i = 1 To $fileList[0]

	$filename = $fileList[$i]
	$strippedFilename = StringStripWS($filename, 8)

	if not FileExists($basePath&$strippedFilename) Then
		ConsoleWrite($fileList[$i]&@CRLF)
		FileCopy($basePath&"bak\"&$filename, $basePath&$strippedFilename)
		sleep(1000)
		$runstring = 'C:\Program Files\Neurocoach-dev\bioera.exe -design ntlgroup\protocols\'&$strippedFilename
		ConsoleWrite($runstring&@CRLF)
		Run($runstring, "C:\Program Files\Neurocoach-dev");
		sleep(5000)

		#region ---Au3Recorder generated code Start (v3.3.7.0)  ---
		_WinWaitActivate("BioEra 3.055","")
		ConsoleWrite($fileList[$i]&@CRLF)
		MouseClick("left",36, -5,1)
		MouseClick("left",36, 75,1)
		MouseClick("left",36, -5,1)
		MouseClick("left",36, 290,1)

		#endregion --- Au3Recorder generated code End ---

		FileCopy($basePath&$strippedFilename, $basePath&"bak\"&$filename)

		sleep(1500)
	EndIf


Next



Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc


