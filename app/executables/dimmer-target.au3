#include <WindowsConstants.au3>
#include <WinAPI.au3>
#include <Constants.au3>
#include <GuiConstants.au3>
#include <StaticConstants.au3>
#include <EDITCONSTANTS.AU3>
#include <MISC.AU3>
#Include <GuiEdit.au3>
#Include <GuiMenu.au3>
#Include <GuiComboBox.au3>
#Include <DATE.au3>
#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <Array.au3>
;===============================================================================
;
; Description:      Window Finder
; Author(s):        Dmitry Yudin (Lazycat)
; Date:             26.10.2007
; Notes:            Used for learning:
;                   http://www.codeproject.com/dialog/windowfinder.asp
;===============================================================================


$iniFile = @ScriptDir&"\client_id.ini"
$clientID = IniRead($iniFile, 'neurocoach', 'clientId', '65500170')

; Setup Network Client
; Opt ('GUIoneventmode', 1)
$ip = '127.0.0.1'
$port = 7489
TCPStartup()
For $0 = 0 To 10
    $Socket = TCPConnect($ip, $port)
    ;Connects to an open socket on the server...
    If $Socket <> -1 Then ExitLoop
    TCPCloseSocket($Socket)
    Sleep(300)

Next
If $Socket = -1 Then _Exit ()

#Region ### START Cursor Graphics ### Form=
; Initializing resources
Global $CURSOR_TARGET = WriteResource( _
"0x000002000100202000000F001000300100001600000028000000200000004000000001000100000000008000" & _
"00000000000000000000020000000200000000000000FFFFFF0000000000000000000000000000000000000000" & _
"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" & _
"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" & _
"00000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" & _
"FFFFFFF83FFFFFE6CFFFFFD837FFFFBEFBFFFF783DFFFF7EFDFFFEAC6AFFFEABAAFFFE0280FFFEABAAFFFEAC6A" & _
"FFFF7EFDFFFF783DFFFFBEFBFFFFD837FFFFE6CFFFFFF83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" & _
"FFFFFFFFFFFFFFFFFFFFFFFF")
Global $ICON_TARGET_FULL = WriteResource( _
"0x0000010001002020080000000000E80200001600000028000000200000004000000001000400000000000002" & _
"000000000000000000001000000010000000000000000000800000800000008080008000000080008000808000" & _
"00C0C0C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000000000000000" & _
"00000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFF" & _
"FFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFF00000FFFFFFFFFFFF000FFFFFFFFFF00FF0FF00FFFFFFFFFF000FF" & _
"FFFFFFF0FF00000FF0FFFFFFFFF000FFFFFFFF0FFFFF0FFFFF0FFFFFFFF000FFFFFFF0FFFF00000FFFF0FFFFFF" & _
"F000FFFFFFF0FFFFFF0FFFFFF0FFFFFFF000FFFFFF0F0F0FF000FF0F0F0FFFFFF000FFFFFF0F0F0F0FFF0F0F0F" & _
"0FFFFFF000FFFFFF0000000F0F0000000FFFFFF000FFFFFF0F0F0F0FFF0F0F0F0FFFFFF000FFFFFF0F0F0FF000" & _
"FF0F0F0FFFFFF000FFFFFFF0FFFFFF0FFFFFF0FFFFFFF000FFFFFFF0FFFF00000FFFF0FFFFFFF000FFFFFFFF0F" & _
"FFFF0FFFFF0FFFFFFFF000FFFFFFFFF0FF00000FF0FFFFFFFFF000FFFFFFFFFF00FF0FF00FFFFFFFFFF000FFFF" & _
"FFFFFFFF00000FFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0" & _
"00FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000007770CCCCCCCCCCCCCCCCCCCC" & _
"C07770007070CCCCCCCCCCCCCCCCCCCCC07070007770CCCCCCCCCCCCCCCCCCCCC0777000000000000000000000" & _
"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" & _
"000000000000000000FFFFFFFF8000000080000000800000008000000080000000800000008000000080000000" & _
"800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080" & _
"0000008000000080000000800000008000000080000000800000008000000080000000FFFFFFFFFFFFFFFFFFFF" & _
"FFFF")
Global $ICON_TARGET_EMPTY = WriteResource( _
"0x0000010001002020080000000000E80200001600000028000000200000004000000001000400000000000002" & _
"000000000000000000001000000010000000000000000000800000800000008080008000000080008000808000" & _
"00C0C0C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000000000000000" & _
"00000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFF" & _
"FFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FF" & _
"FFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFF" & _
"F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFF" & _
"FFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFF" & _
"FFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFF" & _
"FFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFF" & _
"FFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0" & _
"00FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000007770CCCCCCCCCCCCCCCCCCCC" & _
"C07770007070CCCCCCCCCCCCCCCCCCCCC07070007770CCCCCCCCCCCCCCCCCCCCC0777000000000000000000000" & _
"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" & _
"000000000000000000FFFFFFFF8000000080000000800000008000000080000000800000008000000080000000" & _
"800000008000000080000000800000008000000080000000800000008000000080000000800000008000000080" & _
"0000008000000080000000800000008000000080000000800000008000000080000000FFFFFFFFFFFFFFFFFFFF" & _
"FFFF")

; Loading cursor from file
$hTargetCursor = DllCall("User32.dll", "int", "LoadCursorFromFile", "str", $CURSOR_TARGET)
$hTargetCursor = $hTargetCursor[0]
Global $g_StartSearch = False, $gFoundWindow = 0, $gOldCursor

#EndRegion ### END Cursor Graphics ###

; Register Mouse Functions
GUIRegisterMsg ($WM_MOUSEMOVE, "WM_MOUSEMOVE_FUNC")
GUIRegisterMsg ($WM_LBUTTONUP, "WM_LBUTTONUP_FUNC")

; Setup Feedback Timer for stop/go signals from NC instead of a/w hotkeys
Const $FEEDBACK_NONE = 0
Const $FEEDBACK_DIM = 1
Const $FEEDBACK_VLC = 2
Const $FEEDBACK_TRACKMANIA = 3
Const $FEEDBACK_DIGITS = 1
Const $FEEDBACK_ARROWS = 1
Const $FEEDBACK_NETFLIX = 4

Global $feedbackType = $FEEDBACK_DIM ; Dimmmer
Global $isInThreshold = True;
Global $feedbackTickTimer = TimerInit()
Global $msToSendFeedback = 20

; Dimmer Global Variables
Global $isActive = False;
Global $currentTransparency = 255;
Global $transparencyChangeAmount = 1; at 50 ticks per second, we should fully dim / undim in about 2 seconds.

; Window Selection
Global $currentWindowHandle, $currentWindowName, $openWindowString
$currentWindowHandle = 'asdf'
$currentWindowName = 'asdf'
$openWindowString = 'asdf' ; Used to fill combo-box
Global $windowComboBoxUpdateTimer = TimerInit()
Global $msToUpdateComboBox = 500

#Region ### START Koda GUI section ### Form=
;~ $hGUI = GUICreate("Neurocoach Feedback Controller", 568, 334, @DesktopWidth-588, @DesktopHeight-384, -1, BitOR($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
;~ GUICtrlCreateGroup("", -99, -99, 1, 1)
;~ GUICtrlCreateGroup("Drop target on a window", 8, 10, 150, 65)
;~ $hTargetPic = GUICtrlCreateIcon($ICON_TARGET_FULL, 0, 24, 30, 32, 32, BitOR($SS_NOTIFY,$WS_GROUP))
;~ GUICtrlCreateGroup("", -99, -99, 1, 1)
;~ $hSpeedUp = GUICtrlCreateButton("+", 80, 34, 24,24)
;~ $hSpeedDown = GUICtrlCreateButton("-", 130, 34, 24,24)
;~ $hSpeedLabel = GUICtrlCreateLabel("4", 110, 40)
;~ $hOK = GUICtrlCreateButton("Close", 84, 82, 75, 25, 0)
#Region ### START Koda GUI section ### Form=C:\Program Files\Neurocoach-dev\scripts\NCFeedbackControlGUI.kxf
$hGUI = GUICreate("Neurocoach Feedback Controller", 376, 193, @DesktopWidth - 396, @DesktopHeight - 280, -1, BitOR($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
$Group1 = GUICtrlCreateGroup(" Dimmer Options ", 8, 32, 361, 89)
Global $dimTargetComboBox = GUICtrlCreateCombo("", 72, 82, 161, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL))
UpdateWindowComboBox()
$dimTargetLabel = GUICtrlCreateLabel("Target", 16, 52, 35, 17)
$DimSpeedLabel = GUICtrlCreateLabel("Speed:", 272, 56, 54, 20)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
$hSpeedUp = GUICtrlCreateButton("Faster", 248, 80, 51, 25)
$hSpeedDown = GUICtrlCreateButton("Slower", 304, 80, 51, 25)
$hSpeedLabel = GUICtrlCreateLabel("4", 328, 56, 20, 20)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
GUICtrlCreateGroup("", -99, -99, 1, 1)
$feedbackTypeLabel = GUICtrlCreateLabel("Current Mode:", 8, 8, 118, 24)
GUICtrlSetFont(-1, 12, 800, 0, "MS Sans Serif")
$feedbackTypeValueLabel = GUICtrlCreateLabel("Dimmer", 128, 8, 217, 24)
GUICtrlSetFont(-1, 12, 800, 0, "MS Sans Serif")
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###
$hOK = GUICtrlCreateButton("Close", 281, 138, 75, 25, 0)
$hTargetPic = GUICtrlCreateIcon($ICON_TARGET_FULL, 0, 17, 70, 32, 32, BitOR($SS_NOTIFY,$WS_GROUP))
; Dimming Options

; Feedback Menu
Opt("WinTitleMatchMode", 2)
Global $hFeedbackMenu = GUICtrlCreateMenu("&Feedback")
$hFeedbackMenuItemDimmer = GUICtrlCreateMenuItem("Dimmer (Select Window)", $hFeedbackMenu, 1, 1)
GUICtrlSetState($hFeedbackMenuItemDimmer, $GUI_CHECKED)
$hFeedbackMenuItemDimmerGames = GUICtrlCreateMenuItem("Dimmer Games", $hFeedbackMenu, 2, 1)
$hFeedbackMenuItemDigits = GUICtrlCreateMenuItem("Digits", $hFeedbackMenu, 3, 1)
$hFeedbackMenuItemArrows = GUICtrlCreateMenuItem("Arrows", $hFeedbackMenu, 4, 1)
$hFeedbackMenuItemAntGames = GUICtrlCreateMenuItem("ANT Game", $hFeedbackMenu, 5, 1)
$hFeedbackMenuItemAttentionBiasGames = GUICtrlCreateMenuItem("Attention Bias Games", $hFeedbackMenu, 6, 1)
$hFeedbackMenuItemVLC = GUICtrlCreateMenuItem("VLC Player", $hFeedbackMenu, 7, 1)
$hFeedbackMenuItemTrackmania = GUICtrlCreateMenuItem("Trackmania", $hFeedbackMenu, 8, 1)
$hFeedbackMenuItemNetflix = GUICtrlCreateMenuItem("Netflix", $hFeedbackMenu, 9, 1)
$hFeedbackMenuItemNone = GUICtrlCreateMenuItem("No Training", $hFeedbackMenu, 999, 1)

; Open Menu
; $hOpenMenu = GUICtrlCreateMenu("&Open")


GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###



While 1
	_Recv_From_Server()
    $nMsg = GUIGetMsg()
	; ConsoleWrite("Could not open socket" & @CRLF)
    Switch $nMsg
		Case $GUI_EVENT_CLOSE, $hOK
			_Exit()
        Case $hTargetPic
            $g_StartSearch = True
            DllCall("user32.dll", "hwnd", "SetCapture", "hwnd", $hGUI)
            $gOldCursor = DllCall("user32.dll", "int", "SetCursor", "int", $hTargetCursor)
            If not @error Then $gOldCursor = $gOldCursor[0]
            GUICtrlSetImage($hTargetPic, $ICON_TARGET_EMPTY)
		Case $hSpeedUp
			if $transparencyChangeAmount < 10 Then $transparencyChangeAmount = $transparencyChangeAmount + 0.25
			GUICtrlSetData($hSpeedLabel, $transparencyChangeAmount*4)
		Case $hSpeedDown
			if $transparencyChangeAmount > 0.25 Then $transparencyChangeAmount = $transparencyChangeAmount - 0.25
			GUICtrlSetData($hSpeedLabel, $transparencyChangeAmount*4)

		; Change current window when this combo box is changed
		Case $dimTargetComboBox
			SetDimmerWindow(WinGetHandle(GUICtrlRead($dimTargetComboBox)))
			WinActivate($currentWindowHandle)

		; Feedback Menu Actions
		Case $hFeedbackMenuItemVLC
			CheckFeedbackMenuItem($hFeedbackMenuItemVLC)
			SetFeedbackType($FEEDBACK_VLC)
			if not WinExists("VLC media player") Then
				ConsoleWrite("Opening VLC")
				$parentDir = StringLeft(@scriptDir,StringInStr(@scriptDir,"\",0,-1)-1)
				Run($parentDir&"\DVD\DVDPlayer.exe")
			EndIf

		Case $hFeedbackMenuItemDimmer
			CheckFeedbackMenuItem($hFeedbackMenuItemDimmer)
			SetFeedbackType($FEEDBACK_DIM)

		Case $hFeedbackMenuItemNetflix
			CheckFeedbackMenuItem($hFeedbackMenuItemNetflix)
			SetFeedbackType($FEEDBACK_NETFLIX)

		Case $hFeedbackMenuItemDimmerGames
			CheckFeedbackMenuItem($hFeedbackMenuItemDimmerGames)
			SetFeedbackType($FEEDBACK_DIM)
			$parentDir = StringLeft(@scriptDir,StringInStr(@scriptDir,"\",0,-1)-1)
			Run(@ScriptDir&'\dimmergames.exe')
			WinWaitActive("Brainy Arcade Games")
			SetDimmerWindow(WinGetHandle("Brainy Arcade Games"))
			UpdateWindowComboBox()

		Case $hFeedbackMenuItemDigits
			CheckFeedbackMenuItem($hFeedbackMenuItemDigits)
			SetFeedbackType($FEEDBACK_DIM)
			$parentDir = StringLeft(@scriptDir,StringInStr(@scriptDir,"\",0,-1)-1)
			Run(@ScriptDir&'\digits.exe')
			WinWaitActive("NC Digits")
			SetDimmerWindow(WinGetHandle("NC Digits"))
			UpdateWindowComboBox()

		Case $hFeedbackMenuItemAntGames
			CheckFeedbackMenuItem($hFeedbackMenuItemAntGames)
			SetFeedbackType($FEEDBACK_DIM)
			$parentDir = StringLeft(@scriptDir,StringInStr(@scriptDir,"\",0,-1)-1)
			Run(@ScriptDir&'\antgames.exe ' & $clientID)
			WinWaitActive("ANT Games")
			SetDimmerWindow(WinGetHandle("ANT Games"))
			UpdateWindowComboBox()

		Case $hFeedbackMenuItemAttentionBiasGames
			CheckFeedbackMenuItem($hFeedbackMenuItemAttentionBiasGames)
			SetFeedbackType($FEEDBACK_DIM)
			$parentDir = StringLeft(@scriptDir,StringInStr(@scriptDir,"\",0,-1)-1)
			Run(@ScriptDir&'\attentionBias.exe ' & $clientID)
			WinWaitActive("Attention Bias Activity")
			SetDimmerWindow(WinGetHandle("Attention Bias Activity"))
			UpdateWindowComboBox()

		Case $hFeedbackMenuItemArrows
			CheckFeedbackMenuItem($hFeedbackMenuItemArrows)
			SetFeedbackType($FEEDBACK_DIM)
			$parentDir = StringLeft(@scriptDir,StringInStr(@scriptDir,"\",0,-1)-1)
			Run(@ScriptDir&'\arrows.exe')
			WinWaitActive("NC Arrows")
			SetDimmerWindow(WinGetHandle("NC Arrows"))
			UpdateWindowComboBox()

		Case $hFeedbackMenuItemTrackmania
			CheckFeedbackMenuItem($hFeedbackMenuItemTrackmania)
			SetFeedbackType($FEEDBACK_TRACKMANIA)

		Case $hFeedbackMenuItemNone
			CheckFeedbackMenuItem($hFeedbackMenuItemNone)
			SetFeedbackType($FEEDBACK_NONE)

		; Open Menu Actions

	EndSwitch
	CheckFeedbackTimer()
	CheckToUpdateDimmerWindowComboBox()
	; sleep(100)
Wend

Func _Exit()
	WinSetTrans($currentWindowHandle, '', 255)
	Exit
EndFunc

Func IsVisible($handle)

	$state = WinGetState($handle)
	$isVisible = BitAND($state, 2)
	$isEnabled = BitAND($state, 4)

	if $isVisible And $isEnabled Then Return True

	Return False

EndFunc   ;==>IsVisible

Func UpdateWindowComboBox()
	$windowNameArray = 0
	Dim $windowNameArray[1]

	$openWindows = WinList()
	$arrayKey = 0;

	For $i = 1 to $openWindows[0][0]
		If $openWindows[$i][0] <> "" And IsVisible($openWindows[$i][1]) Then
			ReDim $windowNameArray[$arrayKey + 1]
			$windowNameArray[$arrayKey] = $openWindows[$i][0]
			$arrayKey = $arrayKey + 1;
		EndIf
	Next

	$openWindowString = "|"&_ArrayToString($windowNameArray, "|")
	GUICtrlSetData($dimTargetComboBox, $openWindowString, $currentWindowName)
EndFunc

Func CheckToUpdateDimmerWindowComboBox()
	If _GUICtrlComboBox_GetDroppedState($dimTargetComboBox) Then Return 1
	$diff = TimerDiff($windowComboBoxUpdateTimer)
	if $diff >= $msToUpdateComboBox Then
		$windowComboBoxUpdateTimer = TimerInit()
		UpdateWindowComboBox()
	EndIf

EndFunc

; Socket Communication Functions
Func _Recv_From_Server ()
    $Recv = TCPRecv($Socket, 1000000)
    If $Recv = '' Then Return
	ConsoleWrite($Recv & @CRLF)
	$matches = StringRegExp($Recv, 'Ch1=\"([0-9]).0\"', 1, 1)
	if @error Then Return 1

	if $matches[0] = 1 Then
		$isInThreshold = True
	Else
		$isInThreshold = False
	EndIf
EndFunc   ;==>_Recv_From_Server_
Func _Minn ()
    ;WinSetState ($GUI, '', @SW_MINIMIZE)
EndFunc

; Feedback Control Functions
Func CheckFeedbackTimer()
	$ms = TimerDiff($feedbackTickTimer)
	If $ms >= $msToSendFeedback Then
		if $isInThreshold = True Then
			FeedbackGo()
		Else
			FeedbackStop()
		EndIf
		$feedbackTickTimer = TimerInit()
	EndIf

EndFunc

Func SetFeedbackType($type)
	if $feedbackType = $FEEDBACK_DIM Then WinSetTrans($currentWindowHandle, '', 255)
	$feedbackType = $type
	; Set Type Options
	if $type = $FEEDBACK_DIM Then
		$msToSendFeedback = 20
		GUICtrlSetData($feedbackTypeValueLabel, "Dimmer")
	ElseIf $type = $FEEDBACK_VLC Then
		$msToSendFeedback = 100
		GUICtrlSetData($feedbackTypeValueLabel, "VLC Media Player")
	ElseIf $type = $FEEDBACK_TRACKMANIA Then
		GUICtrlSetData($feedbackTypeValueLabel, "Trackmania")
		$msToSendFeedback = 1
	ElseIf $type = $FEEDBACK_NETFLIX Then
		; Open Netflix
		$msToSendFeedback = 100;
		GUICtrlSetData($feedbackTypeValueLabel, "Netflix")
	EndIf

EndFunc

Func CheckFeedbackMenuItem($handle)
	GUICtrlSetState($handle, $GUI_CHECKED)
EndFunc

Func FeedbackGo()
	Switch $feedbackType

		Case $FEEDBACK_DIM
			DoUnDim()

		Case $FEEDBACK_VLC
			If WinActive("VLC") Then
				Send('a')
			EndIf
		Case $FEEDBACK_NETFLIX
			If WinActive("Netflix") Then
				Send('{PGUP}')
			EndIf

	EndSwitch

EndFunc

Func FeedbackStop()
	Switch $feedbackType

		Case $FEEDBACK_DIM
			DoDim()

		Case $FEEDBACK_TRACKMANIA
			Send("w")

		Case $FEEDBACK_VLC
			If WinActive("VLC") Then
				Send('w')
			EndIf

		Case $FEEDBACK_NETFLIX
			If WinActive("Netflix") Then
				Send('{PGDN}')
			EndIf

	EndSwitch
EndFunc

; Dimming Functions
Func DoDim()
	if $g_StartSearch Then Return 1
	if $currentTransparency - $transparencyChangeAmount <= 1 Then Return 1
	$currentTransparency = $currentTransparency - $transparencyChangeAmount;
	WinSetTrans($currentWindowHandle, '', $currentTransparency)
EndFunc

Func DoUnDim()
	if $g_StartSearch Then Return 1
	if $currentTransparency + $transparencyChangeAmount >= 254 Then Return 1
	$currentTransparency = $currentTransparency + $transparencyChangeAmount;
	WinSetTrans($currentWindowHandle, '', $currentTransparency)
EndFunc

Func SetDimmerWindow($handle)
	WinSetTrans($currentWindowHandle, '', 255)
	$currentWindowHandle = $handle
	$currentWindowName = WinGetTitle($handle)
	$currentTransparency = 255
	WinSetTrans($currentWindowHandle, '', $currentTransparency)
EndFunc

; Misc Functions
Func _Log($string)
	ConsoleWrite($string&@CRLF)
EndFunc



#Region ### Window Target Functions #
Func WM_MOUSEMOVE_FUNC($hWnd, $nMsg, $wParam, $lParam)
    If not $g_StartSearch Then Return 1
    Local $mPos = MouseGetPos()
    ;$hWndUnder = DllCall("user32.dll", "hwnd", "WindowFromPoint", "long", $mPos[0], "long", $mPos[1])
	$tPoint = _WinAPI_GetMousePos()
	$hWndUnder = _WinAPI_WindowFromPoint($tPoint)
	$hParent = _WinAPI_GetAncestor($hWndUnder)
    ;If not @error Then $hWndUnder = $hWndUnder[0]
    If CheckFoundWindow($hWndUnder) Then
		SetDimmerWindow($hWndUnder)
        $gFoundWindow = $hWndUnder
    EndIf
    Return 1
EndFunc

Func WM_LBUTTONUP_FUNC($hWnd, $nMsg, $wParam, $lParam)
    If not $g_StartSearch Then Return 1
    $g_StartSearch = False

	; Release captured cursor
    DllCall("user32.dll", "int", "ReleaseCapture")
    DllCall("user32.dll", "int", "SetCursor", "int", $gOldCursor)
    GUICtrlSetImage($hTargetPic, $ICON_TARGET_FULL)
	WinActivate($currentWindowHandle)
    Return 1
EndFunc

Func CheckFoundWindow($hFoundWnd)
  If $hFoundWnd = $hGUI Then Return False
  If $hFoundWnd = 0 Then Return False
  If $hFoundWnd = $gFoundWindow Then Return False
  If not WinExists($hFoundWnd) Then Return False
  Local $hTemp = DllCall("user32.dll", "hwnd", "GetParent", "hwnd", $hFoundWnd)
  If not @error and $hTemp[0] = $hGUI Then Return False
  Return True
EndFunc

Func GetWindowClass($hWnd)
    $pClassName = DllStructCreate("char[256]")
    DllCall("user32.dll", "int", "GetClassName", "hwnd", $hWnd, "ptr", DllStructGetPtr($pClassName), "int", 255)
    Return DllStructGetData($pClassName, 1)
EndFunc

Func WriteResource($sbStringRes)
    Local $sTempFile
    Do
        $sTempFile = @TempDir & "\temp" & Hex(Random(0, 65535), 4)
    Until not FileExists($sTempFile)
    Local $hFile = FileOpen($sTempFile, 2+16)
    FileWrite($hFile, $sbStringRes)
    FileClose($hFile)
    Return $sTempFile
EndFunc

Func OnAutoitExit()
    If IsDeclared("CURSOR_TARGET") Then
        FileDelete($ICON_TARGET_FULL)
        FileDelete($ICON_TARGET_EMPTY)
        FileDelete($CURSOR_TARGET)
    EndIf
EndFunc
#EndRegion ### Window Target Functions #