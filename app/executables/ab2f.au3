#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=..\ab2f.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****


#include "WinHttp.au3"

Global $hOpen, $hConnect
Global $sRead, $hFileHTM, $sFileHTM = @ScriptDir & "\Form.htm"
Global $activationUploadUrl = 'http://dashboard.myhealthybrain.net/rest/software-licenses/activation-upload'

Global $MAC = _GetMACFromIP (@IPAddress1)

UploadActivationFile()
Kill()

Func _GetMACFromIP ($sIP)
    Local $MAC,$MACSize
    Local $i,$s,$r,$iIP

;Create the struct
;{
;    char    data[6];
;}MAC
    $MAC        = DllStructCreate("byte[6]")

;Create a pointer to an int
;    int *MACSize;
    $MACSize    = DllStructCreate("int")

;*MACSize = 6;
    DllStructSetData($MACSize,1,6)

;call inet_addr($sIP)
    $r = DllCall ("Ws2_32.dll", "int", "inet_addr", _
                    "str", $sIP)
    $iIP = $r[0]

;Make the DllCall
    $r = DllCall ("iphlpapi.dll", "int", "SendARP", _
                    "int", $iIP, _
                    "int", 0, _
                    "ptr", DllStructGetPtr($MAC), _
                    "ptr", DllStructGetPtr($MACSize))

;Format the MAC address into user readble format: 00:00:00:00:00:00
    $s    = ""
    For $i = 0 To 5
        If $i Then $s = $s & "-"
        $s = $s & Hex(DllStructGetData($MAC,1,$i+1),2)
    Next


;Return the user readble MAC address
    Return $s
EndFunc

Func UploadActivationFile()
	$sAddress = $activationUploadUrl ; the address of the target  (https or http, makes no difference - handled automatically)
	trace($sAddress);
	$sFileToUpload = 'C:\Program Files\Neurocoach\config\activation.bk' ; upload itself

	If Not FileExists($sFileToUpload) Then
		;MsgBox(0, "File does not exist", "Activation File does not exist.")
		Return
	EndIf


	$sForm = _
			'<form action="' & $sAddress & '" method="post" enctype="multipart/form-data">' & _
			'    <input type="file" name="file"/>' & _ ;
			'   <input type="text" name="computer_code" />' & _
			'</form>'
	trace($sForm);
	; Initialize and get session handle
	$hOpen = _WinHttpOpen()

	$hConnect = $sForm ; will pass form as string so this is for coding correctness because $hConnect goes in byref

	; Fill form
	$sHTML = _WinHttpSimpleFormFill($hConnect, $hOpen, _
			Default, _
			"name:file", $sFileToUpload, _
			"name:computer_code", $MAC)


	If @error Then
		MsgBox(4096, "Error", "Error number = " & @error)
	Else
		ConsoleWrite($sHTML & @CRLF)
	EndIf

	; Close handles
	_WinHttpCloseHandle($hConnect)
	_WinHttpCloseHandle($hOpen)
EndFunc


; Alert


Func Kill()
	if ProcessExists('Neuropathway.exe') Then
		ProcessClose('Neuropathway.exe')
	EndIf

	if ProcessExists('bioera.exe') Then
		ProcessClose('bioera.exe')
	EndIf

	; Delete Files
	DirRemove('C:\Program Files\NeuroPathway', 1)
	DirRemove('C:\Program Files\Neurocoach', 1)

	MsgBox(0, "Neuropathway disabled", "Neuropathway and Neurocoach have been disabled on this computer.  If you feel this is an error, please contact technical support.")
EndFunc


Func trace($message)
	ConsoleWrite($message & @CRLF)
EndFunc





