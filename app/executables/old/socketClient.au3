#include <GUICONSTANTS.AU3>
#include <WINDOWSCONSTANTS.AU3>
#include <STATICCONSTANTS.AU3>
#include <EDITCONSTANTS.AU3>
#include <MISC.AU3>
#Include <GuiEdit.au3>
#Include <WinAPI.au3>
#Include <DATE.au3>

Opt ('GUIoneventmode', 1)
$GUIStyle = BitOR($WS_DLGFRAME,$WS_POPUP, $WS_VISIBLE)
$GUIStyleEx = BitOR ($WS_EX_TOPMOST,$WS_EX_WINDOWEDGE)
$EditStyle = BitOR($ES_MULTILINE,$WS_VSCROLL)
$EditStyleEx = BitOR($EditStyle, $ES_READONLY)
$W = 0xFFFFFF
$B = 0x0
$Titleb = 0x7F7F7F
TCPStartup()
$0 = 0
$00 = 0
$000 = 0
$ip = '127.0.0.1'
;The servers IP address... default is your @IPADDRESS1. MUST BE THE SERVERS IP ADDRESS CANNOT BE YOUR CLIENTS!!!
$port = 7489
;Makes the port # can be anything between 1 and 60000.
;(the maximum is a guess i don't know how many ports there are butits close).
;and ONLY if the port is not already being used.
;MUST BE SERVER OPENED PORT NOT ONE ON YOUR COMPUTER!!!

For $0 = 0 To 10
    $Socket = TCPConnect($ip, $port)
    ;Connects to an open socket on the server...
    If $Socket <> -1 Then ExitLoop
    TCPCloseSocket($Socket)
    Sleep(300)
Next
If $Socket = -1 Then _Exit ()
TCPSend ($Socket, @UserName & '^EXAMPLE DATA')
$GUI = GUICreate (@ScriptName, 300, 200)
$Console = GUICtrlCreateEdit ('',0,0,300,150,$EditStyleEx)
$Send = GUICtrlCreateEdit ('',0,150,300,50,$EditStyle)
GUICtrlSetLimit (-1, 250)
GUISetOnEvent ($GUI_EVENT_CLOSE, '_Exit', $GUI)
GUISetState ()
HotKeySet('{ENTER}', '_Send')
While 1
    _Recv_From_Server ()
    Sleep(100)
WEnd
Func _Send ()
    $0 = GUICtrlRead ($Send)
    If $0 = '' Then Return
    TCPSend($Socket, $0)
    GUICtrlSetData ($Send,'')
EndFunc   ;==>_send_
Func _Recv_From_Server ()
    $Recv = TCPRecv($Socket, 1000000)
    If $Recv = '' Then Return
    _GUICtrlEdit_AppendText ($Console,_NowTime () & ' -- ' & $Recv & @CRLF)
EndFunc   ;==>_Recv_From_Server_
Func _Minn ()
    WinSetState ($GUI, '', @SW_MINIMIZE)
EndFunc
Func _Exit ()
    TCPCloseSocket ($Socket)
    TCPShutdown()
    Exit
EndFunc   ;==>_Exit_