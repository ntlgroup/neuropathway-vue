#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Res_Comment=http://www.hiddensoft.com/autoit3/compiled.html
#AutoIt3Wrapper_Res_Description=AutoIt v3 Compiled Script
#AutoIt3Wrapper_Run_AU3Check=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#Region converted Directives from C:\Program Files\Neurocoach-dev\DimmerGames\runGame.au3.ini
#EndRegion converted Directives from C:\Program Files\Neurocoach-dev\DimmerGames\runGame.au3.ini
;
#include <guiconstants.au3>
#include <misc.au3>

_inetOff()

$left = (@DesktopWidth / 2) - 275
$top = (@DesktopHeight / 2) - 200

$ioRP = ObjCreate("ShockwaveFlash.ShockwaveFlash.1")
$i = 9

;~ if IsObj($ioRP) = 0 Then

;~ 	Do
;~ 		$i = $i - 1
;~ 		$ioRP = ObjCreate("ShockwaveFlash.ShockwaveFlash." & $i)
;~ 	Until IsObj($ioRP) = 1

;~ EndIf


$iSinkObject=ObjEvent($ioRP,"sampleFlaMovie_") ;
;$gameFile = $cmdLine[1]



$iisObj = IsObj($ioRP)
ConsoleWrite($iisObj&@CRLF)

GuiCreate("Brainy Arcade Games",571,471,$left,$top)
;$resetBtn = GUICtrlCreateButton("Reset Game",240,411,90,20)
$gameMenu = GUICtrlCreateMenu("Games")
$starBallBtn = GUICtrlCreateMenuitem("Star Ball", $gameMenu)
$backwardsTetrisBtn = GUICtrlCreateMenuitem("Backwards Tetris", $gameMenu)
$fancyPantsBtn = GUICtrlCreateMenuitem("Mr. Fancy Pants", $gameMenu)
$pacmanBtn = GUICtrlCreateMenuitem("Pacman", $gameMenu)
$simonBtn = GUICtrlCreateMenuitem("Simon", $gameMenu)
$tetrisBtn = GUICtrlCreateMenuitem("Tetris", $gameMenu)

$restartBtn = GUiCtrlCreateButton("Restart", 10, 10)

;Run("Dimmer.exe",@ScriptDir)
$GUIActiveX    = GUICtrlCreateObj( $ioRP, 0, 40 , 570 , 410 )
$iLinkoRP = ObjEvent($ioRP,"IEEvent_","Preview"); this is a dummy event handle
Global $currentMovie
With $ioRP; Object tag pool
    .Movie = @ScriptDir&"\Intro.swf"; the wave file from inet / could be on disk
    .ScaleMode = 0;0 showall, 1 noborder, 2 exactFit, 3 noscale
    .bgcolor = "#FFFFFF"; change background color to white FFFFFF
    .Loop = True
    .wmode = "transparent"; Opaque / transparent
	.Play
EndWith

GUISetState ();Show GUI

While 1
	$msg = GUIGetMsg()

	Select
		Case $msg = $GUI_EVENT_CLOSE
			_inetOn()
			ProcessClose("dimmer.exe")
			ProcessClose("runGame.exe");
			;ProcessClose("GlovePIE.exe")

			Exit

		Case $msg = $restartBtn
			$ioRP.Movie = @ScriptDir&'\Intro.swf'
			$ioRP.bgcolor = "#000000"
			$ioRP.Play
			$ioRP.Movie = $currentMovie
			$ioRP.bgcolor = "#000000"
			$ioRP.Play

		Case $msg = $starBallBtn
			_glovepie($msg)
			$currentMovie = @ScriptDir & "\star_ball.swf"
			$ioRP.Movie = @ScriptDir & "\star_ball.swf"
			$ioRP.bgcolor = "#000000"
			$ioRP.Play
			_restartDimmer()


		Case $msg = $backwardsTetrisBtn
			_glovepie($msg)
			$currentMovie = @ScriptDir & "\backwardstetris.swf"
			$ioRP.Movie = @ScriptDir & "\backwardstetris.swf"
			$ioRP.bgcolor = "#000000"
			_restartDimmer()


		Case $msg = $fancyPantsBtn
			_glovepie($msg)
			$currentMovie = @ScriptDir & "\hedgehog.swf"
			$ioRP.Movie = @ScriptDir & "\hedgehog.swf"
			$ioRP.bgcolor = "#000000"
			_restartDimmer()


		Case $msg = $pacmanBtn
			_glovepie($msg)
			$currentMovie = @ScriptDir & "\Pacman v1.swf"
			$ioRP.Movie = @ScriptDir & "\Pacman v1.swf"
			$ioRP.bgcolor = "#000000"
			_restartDimmer()


		Case $msg = $simonBtn
			_glovepie($msg)
			$currentMovie = @ScriptDir & "\simon.swf"
			$ioRP.Movie = @ScriptDir & "\simon.swf"
			$ioRP.bgcolor = "#000000"
			_restartDimmer()


		Case $msg = $tetrisBtn
			_glovepie($msg)
			$currentMovie = @ScriptDir & "\tetris.swf"
			$ioRP.Movie = @ScriptDir & "\tetris.swf"
			$ioRP.bgcolor = "#000000"
			_restartDimmer()

	EndSelect

WEnd



Func _restartDimmer()

 ;ProcessClose("dimmer.exe")
 ;Run(@ScriptDir & "\Dimmer.exe")

EndFunc

Func _glovepie($amsg)

	If $amsg = false Then
		;ProcessClose("GlovePIE.exe")
	Else

		;ProcessClose("Glovepie.exe")

		Select

		Case $amsg = $starBallBtn
			send("{F9 DOWN}")
			sleep(1000)
			send("{F9 UP}")

		Case $amsg = $backwardsTetrisBtn
			send("{F9 DOWN}")
			sleep(1000)
			send("{F9 UP}")

		Case $amsg = $tetrisBtn
			send("{F9 DOWN}")
			sleep(1000)
			send("{F9 UP}")

		Case $amsg = $pacmanBtn
			send("{F12 DOWN}")
			sleep(1000)
			send("{F12 UP}")

		Case $amsg = $simonBtn
			send("{F9 DOWN}")
			sleep(1000)
			send("{F9 UP}")

		Case $amsg = $fancyPantsBtn
			send("{F12 DOWN}")
			sleep(1000)
			send("{F12 UP}")

		EndSelect

	EndIf

EndFunc


Func _inetOff()
	FileCopy(@HomeDrive & "\Program Files\Mozilla Firefox\firefox.exe",@HomeDrive & "\Program Files\Mozilla Firefox\firefox2.exe")
	FileDelete(@HomeDrive & "\Program Files\Mozilla Firefox\firefox.exe")
EndFunc

Func _inetOn()
	FileCopy(@HomeDrive & "\Program Files\Mozilla Firefox\firefox2.exe",@HomeDrive & "\Program Files\Mozilla Firefox\firefox.exe")
	FileDelete(@HomeDrive & "\Program Files\Mozilla Firefox\firefox2.exe")
EndFunc





