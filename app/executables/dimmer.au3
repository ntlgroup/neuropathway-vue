#include <GUIConstants.au3>
#include <Misc.au3>
$dll = DllOpen("user32.dll")

Global $transChanged
$timeFullShift = 3000 ;milliseconds in which to have full transparency
$trans =  254 ;transparency number, between 0 and 255
$transpercent = $trans / 255 ;transparency, in % form
$transIncrement = 1.2 ;Incrememnt to raise/lower transparency
$window = "Brainy Arcade Game"
$key = "57"
WinSetTrans($window,"",255)

;first calibration
Opt("TrayIconHide", 1)          ;0=show, 1=hide tray icon



$i = 1
While $i = 1 ;first calibration complete
	
	If _IsPressed($key,$dll) Then
		Do
			$begin = TimerInit()
			$trans = 254
			Do			
				$trans = $trans - $transIncrement
				WinSetTrans($window,"",$trans)
				ConsoleWrite("Trans: "&$trans&@CRLF)
				$transpercent = $trans / 255
				ConsoleWrite("TransPercent: " & $transpercent & @CRLF)
				Sleep(10)
			Until $transpercent < .5
			
			WinSetTrans($window,"",254)
			
			$timePercent = TimerDiff($begin)
			ConsoleWrite(@CRLF&"TimePercent: " & $timePercent & @CRLF)
			
			If $timePercent < ($timeFullShift / 2) - 201 Then
				$transIncrement = $transIncrement - .4
			ElseIf $timePercent > ($timeFullShift / 2) + 201 Then
				$transIncrement = $transIncrement + .4
			Elseif $timePercent > ($timeFullShift / 2) - 400 and $timePercent < ($timeFullShift / 2) + 400 Then
				$i = 0
			EndIf
			
			
		Until $timePercent > 1200 and $timePercent < 1800	
		
		$i = 0
	
	EndIf	
WEnd
$calibrated = 0
Do
		$calTimer = TimerInit() ;Start Init Timer
		$aTrans = $trans
		Do
			$trans = $trans + $transIncrement
			If $trans > 254 Then
				$trans = 254
			EndIf
			WinSetTrans($window,"",$trans)
			$timerDiff = TimerDiff($calTimer)
		Until $timerDiff > 100
		$bTrans = $trans
		
		$transChanged = ($bTrans - $aTrans) 
		$masterChange = $transChanged
		$calibrated = 1
		
		
Until $calibrated = 1 or $trans > 253

$uptimer = TimerInit() ;init uptimer
; start continuous Calibration
$downCounter = 0
$uncal = 1
While 1
	
	If _IsPressed($key ,$dll) Then ; Function DIM, start Downtimer
		_dim()
		$downCounter = $downCounter + 1
		$uptimer = TimerInit()
		$uncal = 1
	Else ; Function UnDim, start Uptimer
		
		
		_undim()
	EndIf
	
	If $downCounter > 80 and TimerDiff($uptimer) > 100 Then ;runs _calibrate() when program undims 
		
		If _IsPressed($key,$dll) Then
		Else
			If $trans > 253 Then
			Else
				If $uncal = 0 Then 
				Else					
				$uncal = _calibrate()
				EndIf
			EndIf
			
		EndIf
		$downCounter = 0
	EndIf
		
WEnd



Func _dim()
	$trans = $trans - $transIncrement
	If $trans < 1 Then
		$trans = 1
	EndIf
	WinSetTrans($window,"",$trans)
EndFunc



Func _undim()
	$trans = $trans + $transIncrement
	If $trans > 254 Then
		$trans = 254
	EndIf
	WinSetTrans($window,"",$trans)
EndFunc



Func _calibrate()
	ConsoleWrite("Calibrate Now")
	$calibrated = 0
	Do
		$calTimer = TimerInit() ;Start Init Timer
		$aTrans = $trans
		Do
			$trans = $trans + $transIncrement
			If $trans > 254 Then
				$trans = 254
			EndIf
			WinSetTrans($window,"",$trans)
			$timerDiff = TimerDiff($calTimer)
		Until $timerDiff > 100
		$bTrans = $trans
		
		$transChanged = ($bTrans - $aTrans) 
		
		If $transChanged > $masterChange  Then
			$transIncrement = $transIncrement - 1
		Elseif $transChanged < $masterChange - 5 Then
			$transIncrement = $transIncrement + .4
		EndIf
		
		If $transChanged < $masterChange + 5 and $transChanged > $masterChange - 5 Then
			$calibrated = 1
			ConsoleWrite("Calibrated = 1" & @CRLF)
		EndIf
		
		ConsoleWrite(@CRLF & "TransChanged = " & $transChanged & @CRLF)
		
		
	Until $calibrated = 1 or $trans > 253
	
	Return 0	
EndFunc

