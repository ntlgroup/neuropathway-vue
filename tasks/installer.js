var installer = require('electron-installer-windows')

var options = {
    src: 'builds/neuropathway-win32-ia32',
    dest: 'builds/installers/'
}

console.log('Creating package (this may take a while)')

installer(options, function (err) {
    if (err) {
        console.error(err, err.stack)
        process.exit(1)
    }

    console.log('Successfully created package at ' + options.dest)
})


// var electronInstaller = require('electron-winstaller');
//
// resultPromise = electronInstaller.createWindowsInstaller({
//     appDirectory: './builds/neuropathway-win32-ia32',
//     outputDirectory: './builds/installers',
//     authors: 'Ntl Group',
//     exe: 'neuropathway.exe'
// });
//
// resultPromise.then(() => console.log("It worked!"), (e) => console.log(`No dice: ${e.message}`));